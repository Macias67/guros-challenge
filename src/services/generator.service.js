/**
 *
 * @returns {string[]} dna
 */
const randomDNA = () => {
  const characters = 'ATCG';
  const charactersLength = characters.length;
  const dna = [];
  for (let d = 0; d < 6; d++) {
    let result = '';
    for (let i = 0; i < 6; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    dna[d] = result;
  }

  return dna;
};

module.exports = {
  randomDNA,
};
