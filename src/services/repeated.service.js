/**
 *
 * @returns {boolean}
 * @param {array<string>} chars
 */
const isAllEquals = (chars) => {
  let counter = 0;
  // eslint-disable-next-line no-restricted-syntax
  for (const element of chars) {
    if (chars[0] === element) {
      counter += 1;
    }
  }
  return counter === chars.length;
};

module.exports = {
  isAllEquals,
};
