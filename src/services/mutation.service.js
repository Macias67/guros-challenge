/* eslint-disable max-len,no-plusplus */
const { memoize } = require('./memoize.service');
const { isAllEquals } = require('./repeated.service');
const { hasher } = require('./hasher.service');

/**
 * @param {array<string>} dna
 * @returns {boolean}
 */
const hasMutation = (dna) => {
  const matrix = dna.map((e) => e.split(''));
  // Apply memoization <3
  const isAllEqualsMemoized = memoize(isAllEquals);
  // Collect sequences
  const setSequence = new Set();
  // Create a safer path to compare all chars, this creates only a path for a 4x4 matrix
  const maxLooping = 3;
  for (let y = 0; y < maxLooping; y++) {
    for (let x = 0; x < maxLooping; x++) {
      // Start to verify first oblique lines for each quadrant
      const oblique1 = [matrix[y][x], matrix[y + 1][x + 1], matrix[y + 2][x + 2], matrix[y + 3][x + 3]];
      const oblique2 = [matrix[y][x + 3], matrix[y + 1][x + 2], matrix[y + 2][x + 1], matrix[y + 3][x]];

      if (y === 0 && x === 0 && isAllEqualsMemoized(oblique1)) {
        const coords = [y, x, y + 1, x + 1, y + 2, x + 2, y + 3, x + 3];
        setSequence.add(hasher(coords.join('')));
      }

      const oblique1Coords = hasher([y, x, y + 1, x + 1, y + 2, x + 2, y + 3, x + 3].join(''));
      if (isAllEqualsMemoized(oblique1) && !setSequence.has(oblique1Coords)) {
        setSequence.add(oblique1Coords);
        if (setSequence.size === 2) {
          return true;
        }
      }

      const oblique2Coords = hasher([y, x + 3, y + 1, x + 2, y + 2, x + 1, y + 3, x].join(''));
      if (isAllEqualsMemoized(oblique2) && !setSequence.has(oblique2Coords)) {
        setSequence.add(oblique2Coords);
        if (setSequence.size === 2) {
          return true;
        }
      }

      /**
       * Is important to notice, the first quadrant check, verifies the most part
       * of the matrix, so we can optimize the next iterations with memoization
       */
      for (let k = 0; k < 4; k++) {
        const horizontal = [matrix[y + k][x], matrix[y + k][x + 1], matrix[y + k][x + 2], matrix[y + k][x + 3]];
        const vertical = [matrix[y][x + k], matrix[y + 1][x + k], matrix[y + 2][x + k], matrix[y + 3][x + k]];

        const horizontalCoords = hasher([y + k, x, y + k, x + 1, y + k, x + 2, y + k, x + 3].join(''));
        if (isAllEqualsMemoized(horizontal) && !setSequence.has(horizontalCoords)) {
          setSequence.add(horizontalCoords);
          if (setSequence.size === 2) {
            return true;
          }
        }

        const verticalCoords = hasher([y, x + k, y + 1, x + k, y + 2, x + k, y + 3, x + k].join(''));
        if (isAllEqualsMemoized(vertical) && !setSequence.has(verticalCoords)) {
          setSequence.add(verticalCoords);
          if (setSequence.size === 2) {
            return true;
          }
        }
      }
    }
  }

  return false;
};

/**
 * @param {array<string>} dna
 * @returns {{isMutated: boolean}}
 */
const hasMutationDetails = (dna) => {
  const matrix = dna.map((e) => e.split(''));
  // Apply memoization <3
  const isAllEqualsMemoized = memoize(isAllEquals);
  // Create a safer path to compare all chars, this creates only a path for a 4x4 matrix
  const maxLooping = 3;

  for (let y = 0; y < maxLooping; y++) {
    for (let x = 0; x < maxLooping; x++) {
      // Start to verify first oblique lines for each quadrant
      const oblique1 = [matrix[y][x], matrix[y + 1][x + 1], matrix[y + 2][x + 2], matrix[y + 3][x + 3]];
      if (isAllEqualsMemoized(oblique1)) {
        return {
          isMutated: true,
          type: 'oblique',
          line: oblique1,
          coords: [
            [x, y],
            [x + 1, y + 1],
            [x + 2, y + 2],
            [x + 3, y + 3],
          ],
        };
      }

      const oblique2 = [matrix[y][x + 3], matrix[y + 1][x + 2], matrix[y + 2][x + 1], matrix[y + 3][x]];
      if (isAllEqualsMemoized(oblique2)) {
        return {
          isMutated: true,
          type: 'oblique',
          line: oblique2,
          coords: [
            [x + 3, y],
            [x + 2, y + 1],
            [x + 1, y + 2],
            [x, y + 3],
          ],
        };
      }

      /**
       * Is important to notice, the first quadrant check, verifies the most part
       * of the matrix, so we can optimize the next iterations with memoization
       */
      for (let i = 0; i < 4; i++) {
        const horizontal = [matrix[y + i][x], matrix[y + i][x + 1], matrix[y + i][x + 2], matrix[y + i][x + 3]];

        if (isAllEqualsMemoized(horizontal)) {
          return {
            isMutated: true,
            type: 'horizontal',
            line: horizontal,
            coords: [
              [x, y + i],
              [x + 1, y + i],
              [x + 2, y + i],
              [x + 3, y + i],
            ],
          };
        }

        const vertical = [matrix[y][x + i], matrix[y + 1][x + i], matrix[y + 2][x + i], matrix[y + 3][x + i]];
        if (isAllEqualsMemoized(vertical)) {
          return {
            isMutated: true,
            type: 'vertical',
            line: vertical,
            coords: [
              [x + i, y],
              [x + i, y + 1],
              [x + i, y + 2],
              [x + i, y + 3],
            ],
          };
        }
      }
    }
  }

  return {
    isMutated: false,
  };
};

module.exports = {
  hasMutation,
  hasMutationDetails,
};
