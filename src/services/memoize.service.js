/**
 * @param fn Function to decorate
 * @returns {function(...[*]): *}
 */
const memoize = (fn) => {
  const cache = {};
  return (...args) => {
    const key = JSON.stringify(args[0].join(''));
    cache[key] = typeof cache[key] === 'undefined' ? fn(...args) : cache[key];
    return cache[key];
  };
};

module.exports = {
  memoize,
};
