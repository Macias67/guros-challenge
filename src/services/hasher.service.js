const { createHash } = require('crypto');

/**
 *
 * @param string
 * @returns {string}
 */
const hasher = (string) => createHash('sha256').update(string).digest('hex');

module.exports.hasher = hasher;
