/* eslint-disable max-len */
// eslint-disable-next-line import/no-extraneous-dependencies
const DynamoDB = require('aws-sdk/clients/dynamodb');
const { createHash } = require('crypto');

const dynamodb = new DynamoDB.DocumentClient();

/**
 *
 * @param string
 * @returns {string}
 */
const hasher = (string) => createHash('sha256').update(string).digest('hex');

/**
 *
 * @param {array<string>} dna
 * @param {boolean} mutated
 * @returns {Promise<void>}
 */
const upsertDnaRegister = async (dna, mutated) => {
  const hashId = hasher(dna.toString());

  const { Atrributes } = await dynamodb.update({
    TableName: 'dna_stats',
    Key: { hashId },
    ExpressionAttributeValues: {
      ':vector': dna,
      ':mutated': mutated,
      ':updated_at': new Date().toISOString(),
    },
    // ExpressionAttributeNames: { '#hash': 'hash' },
    UpdateExpression: 'SET vector = :vector, mutated = :mutated, updated_at = :updated_at',
    ReturnValues: 'ALL_NEW',
  }).promise();

  return Atrributes;
};

/**
 *
 * @returns {Promise<{count_mutations: DocumentClient.Integer, count_no_mutation: DocumentClient.Integer, ratio: number}>}
 */
const getStats = async () => {
  const { Count: countMutated } = await dynamodb.scan({
    TableName: 'dna_stats',
    ScanIndexForward: true,
    FilterExpression: '#mutated = :mutated',
    ExpressionAttributeNames: {
      '#mutated': 'mutated',
    },
    ExpressionAttributeValues: {
      ':mutated': true,
    },
  }).promise();

  const { Count: countNoMutated } = await dynamodb.scan({
    TableName: 'dna_stats',
    ScanIndexForward: true,
    FilterExpression: '#mutated = :mutated',
    ExpressionAttributeNames: {
      '#mutated': 'mutated',
    },
    ExpressionAttributeValues: {
      ':mutated': false,
    },
  }).promise();

  return {
    count_mutations: countMutated,
    count_no_mutation: countNoMutated,
    ratio: parseFloat((countMutated / countNoMutated).toFixed(1)),
  };
};

module.exports = {
  upsertDnaRegister,
  getStats,
};
