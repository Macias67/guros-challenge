/* eslint-disable camelcase */
const dnaRegister = require('../services/dnaRegister.service');

module.exports.index = async () => {
  const response = {
    statusCode: 200,
  };

  try {
    const {
      count_mutations,
      count_no_mutation,
      ratio,
    } = await dnaRegister.getStats();

    response.body = JSON.stringify({ count_mutations, count_no_mutation, ratio });

    return response;
  } catch (e) {
    response.statusCode = 403;
    response.body = JSON.stringify({ message: e.message, error: e });

    return response;
  }
};
