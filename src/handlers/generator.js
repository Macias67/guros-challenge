/* eslint-disable camelcase */
const { randomDNA } = require('../services/generator.service');

module.exports.index = async () => {
  const response = {
    statusCode: 200,
  };

  try {
    const dna = randomDNA();
    response.body = JSON.stringify({ dna }, null, 2);
    return response;
  } catch (e) {
    response.statusCode = 403;
    response.body = JSON.stringify({ message: e.message, error: e });

    return response;
  }
};
