const { hasMutation } = require('../services/mutation.service');
const dnaRegister = require('../services/dnaRegister.service');

module.exports.index = async (event) => {
  const response = {
    statusCode: 200,
  };

  try {
    const { dna } = JSON.parse(event.body);

    if (!Array.isArray(dna) || dna.length !== 6) {
      throw new Error('Invalid dna');
    }

    const isMutated = hasMutation(dna);
    await dnaRegister.upsertDnaRegister(dna, isMutated);

    response.body = JSON.stringify({ isMutated });
    return response;
  } catch (e) {
    response.statusCode = 403;
    response.body = JSON.stringify({ message: 'Malformed DNA or something went wrong', error: e.message });

    return response;
  }
};
