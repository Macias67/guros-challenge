const { hasMutationDetails } = require('../services/mutation.service');

module.exports.index = async (event) => {
  const response = {
    statusCode: 200,
  };

  try {
    const { dna } = JSON.parse(event.body);

    if (!Array.isArray(dna) || dna.length !== 6) {
      throw new Error('Invalid dna');
    }

    const details = hasMutationDetails(dna);

    response.body = JSON.stringify({ details }, null, 2);
    return response;
  } catch (e) {
    response.statusCode = 403;
    response.body = JSON.stringify({ message: 'Malformed DNA or something went wrong', error: e.message });

    return response;
  }
};
