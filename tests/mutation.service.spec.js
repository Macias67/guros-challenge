const { hasMutation } = require('../src/services/mutation.service');

describe('Mutation Service', () => {
  it('Matrix with 2 sequences', () => {
    const dna = ['GCGCAT', 'AACTCT', 'ACACAT', 'TAAAAA', 'GGACTT', 'CTATAC'];

    expect(hasMutation(dna)).toBe(true);
    expect(hasMutation(dna)).toEqual(true);
  });

  it('Matrix with 2 sequences oblique line', () => {
    const dna = ['GTGTTT', 'TTGCGT', 'CTCCAT', 'GCCGCT', 'GCCCAT', 'TGCCCC'];
    const dna2 = ['GTGATT', 'TGTTGT', 'CATTAA', 'ATCGTT', 'GCCCAT', 'TGCGCC'];

    expect(hasMutation(dna)).toBe(true);
    expect(hasMutation(dna)).toEqual(true);

    expect(hasMutation(dna2)).toBe(true);
    expect(hasMutation(dna2)).toEqual(true);
  });

  it('Matrix with 2 sequences oblique inverted line', () => {
    const dna = ['GTGGTT', 'TTGCGT', 'CGCCAA', 'GCCGCT', 'GCCCAT', 'TGCCCC'];
    const dna2 = ['GTGGTT', 'TTGCGT', 'CGCCAA', 'GCCGCT', 'GCCCAT', 'TGCGCC'];
    const dna3 = ['GTGGTT', 'TGGCGT', 'CGGCAA', 'GCCGCT', 'GCCCAT', 'TGCGCC'];

    expect(hasMutation(dna)).toBe(true);
    expect(hasMutation(dna)).toEqual(true);

    expect(hasMutation(dna2)).toBe(true);
    expect(hasMutation(dna2)).toEqual(true);

    expect(hasMutation(dna3)).toBe(true);
    expect(hasMutation(dna3)).toEqual(true);
  });

  it('No mutated DNA', () => {
    const dna = ['CAAGAT', 'TGGATC', 'ACGATC', 'GAGCCT', 'CCCGAT', 'GGAGCC'];
    const dna2 = ['CAGCGG', 'CCGCCC', 'CATTAT', 'GCACAA', 'TCGGCT', 'TGTGGC'];

    expect(hasMutation(dna)).toBe(false);
    expect(hasMutation(dna)).toEqual(false);

    expect(hasMutation(dna2)).toBe(false);
    expect(hasMutation(dna2)).toEqual(false);
  });
});
