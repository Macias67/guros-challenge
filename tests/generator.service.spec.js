const { randomDNA } = require('../src/services/generator.service');

describe('DNA generator', () => {
  it('Generate array length 6', () => {
    const dna = randomDNA();

    expect(dna).toHaveLength(6);
  });

  const dna = randomDNA();
  test.each(dna)('Well formed for each element', (charDNA) => {
    expect(charDNA).toHaveLength(6);
    expect(charDNA).toEqual(expect.stringMatching(/^[ATCG]*$/));
  });
});
