const { isAllEquals } = require('../src/services/repeated.service');

describe('Function to analyze all characters are equals', () => {
  it('Given an array of different characters, verify that all characters are equal', () => {
    const characters = ['A', 'T', 'G', 'C', 'G', 'A'];
    expect(isAllEquals(characters)).toBe(false);
    expect(isAllEquals(characters)).toEqual(false);
  });

  it('Given an array of characters, verify that all characters are equal', () => {
    const characters = ['T', 'T', 'T', 'T', 'T', 'T'];
    expect(isAllEquals(characters)).toBe(true);
    expect(isAllEquals(characters)).toEqual(true);
    expect(isAllEquals(characters)).toBeTruthy();
  });
});
