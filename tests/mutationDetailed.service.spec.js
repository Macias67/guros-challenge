const { hasMutationDetails } = require('../src/services/mutation.service');

describe('Mutation Service', () => {
  it('Matrix with 4 equals consecutive letters vertically', () => {
    const dna = ['TGTTTA', 'CCCAGT', 'TCGGCT', 'TCACTG', 'GCTGTT', 'CATCTT'];

    expect(hasMutationDetails(dna)).toHaveProperty('isMutated');
    expect(hasMutationDetails(dna)).toHaveProperty('type');
    expect(hasMutationDetails(dna)).toHaveProperty('line');
    expect(hasMutationDetails(dna)).toHaveProperty('coords');
  });

  it('Matrix with 4 equals consecutive letters horizontally', () => {
    const dna = ['CCCCTT', 'TACCCG', 'CGAGCG', 'CCAGAT', 'AAGGCG', 'GAATGA'];

    expect(hasMutationDetails(dna)).toHaveProperty('isMutated');
    expect(hasMutationDetails(dna)).toHaveProperty('type');
    expect(hasMutationDetails(dna)).toHaveProperty('line');
    expect(hasMutationDetails(dna)).toHaveProperty('coords');
  });

  it('Matrix 4 equals consecutive letters obliques LR lines', () => {
    const dna = ['AGAGAG', 'CTGAAC', 'ACTTCG', 'TTATTA', 'GACGTG', 'GCCCTG'];

    expect(hasMutationDetails(dna)).toHaveProperty('isMutated');
    expect(hasMutationDetails(dna)).toHaveProperty('type');
    expect(hasMutationDetails(dna)).toHaveProperty('line');
    expect(hasMutationDetails(dna)).toHaveProperty('coords');
  });

  it('Matrix 4 equals consecutive letters obliques RL lines', () => {
    const dna = ['CCGCGA', 'TCACCC', 'AAGACA', 'CCACTG', 'ATCTCT', 'GTTCCC'];

    expect(hasMutationDetails(dna)).toHaveProperty('isMutated');
    expect(hasMutationDetails(dna)).toHaveProperty('type');
    expect(hasMutationDetails(dna)).toHaveProperty('line');
    expect(hasMutationDetails(dna)).toHaveProperty('coords');
  });

  it('No mutated DNA', () => {
    const dna = ['CAAGAT', 'TGGATC', 'ACGATC', 'GAGCCT', 'CCCGAT', 'GGAGCC'];

    expect(hasMutationDetails(dna)).toHaveProperty('isMutated');
    expect(hasMutationDetails(dna)).not.toHaveProperty('type');
    expect(hasMutationDetails(dna)).not.toHaveProperty('line');
    expect(hasMutationDetails(dna)).not.toHaveProperty('coords');
    expect(hasMutationDetails(dna)).toMatchObject({
      isMutated: false,
    });
  });
});
