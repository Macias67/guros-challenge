const { memoize } = require('../src/services/memoize.service');

describe('Memoize Decorator', () => {
  it('function with array parameters and return a expected value', () => {
    const sum = ([x, y]) => x + y;
    const sumMemoize = memoize(sum);
    expect(sumMemoize([4, 8])).toEqual(12);
    expect(sumMemoize([3, 3])).toEqual(6);
    expect(sumMemoize([6, 1])).toEqual(7);
    expect(sumMemoize([4, -2])).toEqual(2);
  });
});
